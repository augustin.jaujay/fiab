# Recette du logiciel imt_order

## Paramètres modifiables

Simulateur front-end :
* nombre de requêtes par seconde,
* durée de la simulation,
* proportion de requêtes de commande et de requêtes de statistiques,
* durée de traitement des requêtes.

Simulateur back-end :
* nombre de produits,
* nombre de magasins,
* durée de traitement des requêtes.

## Analyse

### Nombre de requêtes par seconde

Cela fait augmenter le nombre de requêtes qui ont échoué. On constate en regardant le fichier "stats.csv" que les requêtes qui échouent sont toujours des requêtes paiement qui arrivent avant la requête de commande et qui ne peuvent donc pas être traitées.

### Durée de la simulation

La durée de la simulation impacte peu son résultat, si ce n'est que la tendance se dessine plus fortement. Le système étant supposé tourner 24h/24, je n'ai pas testé ce paramètre.

### Proportion de requêtes de commande et de requêtes de statistiques

**Lorsqu'on n'envoie que des requêtes de statistiques :** toutes les requêtes réussissent et sont traitées. C'est une opération simple qui a une requête associe une réponse ; il n'y a pas de raisons que ça ne marche pas.

**Lorsqu'on n'envoie que res requêtes de commandes :** le tiers des requêtes échoue. De nouveau, lorsqu'on examine le contenu du fichier "stats.csv", on constate que cela se produit lorsqu'un paiement arrive avant une commande.

### Nombre de produits

On constate que plus ce paramètre augmente, plus la latence augmente : près de trois quart des requêtes prennent plus de 200 ms à être traitées, ce qui est énorme au vu des observations précédentes. On constate également une augmentation de la part des requêtes qui ont échoué : cela vient toujours du même problème, qui est cette fois amplifié par le délai de traitement des requêtes.

### Nombre de magasin

On observe le même phénomène que pour le nombre de produits : le retard induit des chevauchement des requêtes qui en fait échouer une plus grande part.

### Durée de traitement des requêtes

Lorsqu'on augmente le délai de traitement des requêtes, il faut aussi augmenter la durée de la simulation car on a le temps de faire moins de requêtes, puisqu'elles durent plus longtemps. Cette variable a le même effet que les deux précédentes.

## Solutions proposées

Pour régler le problème du paiement qui arrive avant la commande, il faudrait mettre en place un système qui permettrait de mémoriser les requêtes de paiement pour pouvoir les associer à la bonne commande qui arrivera probablement quelques dixièmes de secondes plus tard.

Pour régler le problème d'un trop grand nombre de magasins ou de produits, deux solutions :
* Optimiser le code pour qu'il soit plus efficace lors du traitement d'une grande quantité de données,
* Améliorer la puissance de calcul de serveur qui accueille cette application.
defmodule PingPong do
    def main do
        pid_ping = spawn(__MODULE__, :ping, [])
        pid_pong = spawn(__MODULE__, :pong, [])
        send(pid_pong, {pid_ping, :pong})
        Process.send_after(pid_ping, :stop, 2000)
        Process.send_after(pid_pong, :stop, 2000)
    end

    def ping do
        receive do
            {sender, :ping} ->
                send(sender, {self(), :pong})
                IO.puts("ping")
                ping()
            :stop -> :ok
        end
    end

    def pong do
        receive do
            {sender, :pong} ->
                send(sender, {self(), :ping})
                IO.puts("pong")
                pong()
            :stop -> :ok
        end
    end
end

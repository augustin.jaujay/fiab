defmodule DataServer do
  def main l do
    pid_server = spawn(__MODULE__, :listen_connect, [l])
    send(pid_server, {:connect, "c", self()})
    send(pid_server, {:data, "coucou", self()})
    receive do
      {pid, :ack} -> IO.puts("ca marche")
    end
  end

  def listen_connect l do
    receive do
      {:connect, user, pid} ->
        if user in l do
          IO.puts("Registered user")
          send(pid, {self(), :ack})
          listen_data(l)
        else
          IO.puts("Not registered user")
          send(pid, {self(), :nack})
          listen_connect(l)
        end
      :stop -> :ok
    end
  end

  def listen_data l do
    receive do
      {:data, d, pid} ->
        IO.puts("Data received : " <> d)
        send(pid, {self(), :ack})
        listen_data(l)
      :close ->
        listen_connect(l)
      :stop -> :ok
      end
    end
end

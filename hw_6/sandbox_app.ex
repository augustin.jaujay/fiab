defmodule ImtSandbox.App do
  use Application
  require Logger
  def start(_,_) do
    File.rmdir("data")
    File.mkdir("data")
    Logger.info("[#{Application.fetch_env!(:imt_order, :app_type)}] Starting with configuration #{inspect Application.get_all_env(:imt_order)}")

    app_type = Application.fetch_env!(:imt_order, :app_type)

    case app_type do
      "Front" ->
        Supervisor.start_link([ImtSim.Front], strategy: :one_for_one)
      "Back" ->
        Supervisor.start_link([ImtSim.Back], strategy: :one_for_one)
      _ ->
        Supervisor.start_link([ImtOrder.App], strategy: :one_for_one)
    end
  end
end

defmodule ImtOrder.API do
  use Plug.Router
  #plug Plug.Logger
  plug :match
  plug :dispatch

  get "/aggregate-stats/:product" do
    # IO.puts "get stat for #{product}"

    res =
      ImtOrder.StatsToDb.get(product)
       |> Enum.reduce(%{ca: 0, total_qty: 0}, fn {sold_qty,price}, acc ->
        %{acc|
          ca: acc.ca + sold_qty * price,
          total_qty: acc.total_qty + sold_qty
        }
      end)

    res = Map.put(res, :mean_price, res.ca / (if res.total_qty == 0, do: 1, else: res.total_qty))
    conn |> send_resp(200, Poison.encode!(res)) |> halt()
  end

  put "/stocks" do
    {:ok,bin,conn} = read_body(conn,length: 100_000_000)
    for line<-String.split(bin,"\n") do
      case String.split(line,",") do
        [_,_,_]=l->
          [prod_id,store_id,quantity] = Enum.map(l,&String.to_integer/1)
          MicroDb.HashTable.put("stocks",{store_id,prod_id},quantity)
        _-> :ignore_line
      end
    end
    conn |> send_resp(200,"") |> halt()
  end

  # Choose first store containing all products and send it the order !
  post "/order" do
    {:ok,bin,conn} = read_body(conn)
    order = Poison.decode!(bin)

    case ImtOrder.OrderSupervisor.start_server(order["id"], Node.self) do
      {:new, pid} ->
        # IO.inspect({order["id"], selectedNode, pid})
        case GenServer.call(pid, {:order, order}, 30_000) do
          :ok -> conn |> send_resp(200,"") |> halt()
          error -> conn |> send_resp(500,"Coundn't create order") |> halt()
        end
      _ -> conn |> send_resp(500,"Coundn't create order") |> halt()
      end
    end

    # payment arrived, get order and process package delivery !
    post "/order/:orderid/payment-callback" do
      {:ok,bin,conn} = read_body(conn)
      %{"transaction_id"=> transaction_id} = Poison.decode!(bin)

      orderFound = 1..((Application.fetch_env!(:imt_order, :nodes) |> Enum.count) - 4)
      |> Enum.reduce([], fn index, acc ->
        case ImtOrder.OrderSupervisor.start_server(orderid, :"imt_order_#{index}@127.0.0.1") do
          {:existing, orderPid} -> [orderPid|acc]
          {:new, pid} ->
            ImtOrder.OrderSupervisor.stop_server(pid)
            acc
          :error -> acc
        end
      end)
      |> Enum.reduce(true, fn pid, acc ->
        if is_pid pid do
          case GenServer.call(pid, {:payment, transaction_id, orderid}, 30_000) do
            :ok ->
              ImtOrder.OrderSupervisor.stop_server(pid)
              true and acc
            :ko -> false and acc
            _ -> false and acc
            end
          else true and acc
        end
      end)
      # IO.inspect {orderid, orderFound}
      case orderFound do
        true ->
          conn |> send_resp(200,"") |> halt()
        false ->
          conn |> send_resp(404,"order not found") |> halt()
      end
    end
end

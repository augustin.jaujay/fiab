defmodule FileServer do
  use GenServer

  def init(racine) do
    {:ok, {racine, %{racine => {[], racine}}}}
  end

  def handle_call(:pwd, _from, {current, dict}) do
    {:reply, current, {current, dict}}
  end

  def handle_call(:ls, _from, {current, dict}) do
    {:reply, getStringList(elem(dict[current], 0)), {current, dict}}
  end

  def handle_call({:mkdir, folderName}, _from, {current, dict}) do
    if Map.has_key?(dict, folderName)  do
      Map.put(dict, folderName, {[], current})
      Map.put(dict, current, addChild(folderName, dict[current]))
      {:reply, "Folder added", {current, dict}}
    else
      {:reply, "This name is already used", {current, dict}}
    end
  end

  def terminate(reason, state) do
    0
  end

  def addChild folderName, {list, father} do {[folderName|list], father} end

  def getStringList [] do "" end
  def getStringList [a|q] do a <> " " <> getStringList(q) end
end

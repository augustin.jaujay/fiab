// Démarrage du serveur : les identifiants commenceront à 0
iex(1)> {:ok, pid} = GenServer.start(AlarmServer, 0)   
{:ok, #PID<0.145.0>}

// Enregistrement d'un utilisateur
iex(2)> GenServer.call(pid, {:register, %{:name => "Augustin"}})
{"User registered", 0}

// Enregistrement d'une alarme
iex(3)> GenServer.call(pid, {:set_alarm, self(), "alarm1", fn(_, data) -> data > 10 end})
"Alarm added"

/ Enregistrement d'une alarme : le nom est déjà utilisé et cela provoque un erreur
iex(4)> GenServer.call(pid, {:set_alarm, self(), "alarm1", fn x -> x end})               
{:error, "This name already exists"}

// Ajout d'une donnée qui ne fera pas sonner l'alarme
iex(5)> GenServer.cast(pid, {:data, 0, 7})
:ok

// Ajout d'une donnée qui fera sonner l'alarme
iex(6)> GenServer.cast(pid, {:data, 0, 15})
:ok

// Vérification des alarmes levées
iex(7)> receive do m -> m end
{:alarm, "alarm1", 0, 15}

// Rien à recevoir d'autre
iex(8)> receive do m -> m end

// Recherche des équipements qui feraient lever l'alarme "data < 100" 
// ==> À tester avec plusieurs équipements pour bien voir le fonctionnement
iex(9)> GenServer.call(pid, {:search, fn(_, data) -> data < 100 end})
[0]
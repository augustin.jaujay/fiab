defmodule AlarmServer do
  use GenServer
  @moduledoc """
  Documentation for `AlarmServer`.
  """

  @doc """
  Ce module est un serveur d'alarme tout simple qui implémente le sujet de devoir suivant :
  https://p4s.enstb.org/elixir/#devoir-2

  ## Examples

    // Démarrage du serveur : les identifiants commenceront à 0
    iex(1)> {:ok, pid} = GenServer.start(AlarmServer, 0)
    {:ok, #PID<0.145.0>}

    // Enregistrement d'un utilisateur
    iex(2)> GenServer.call(pid, {:register, %{:name => "Augustin"}})
    {"User registered", 0}

    // Enregistrement d'une alarme
    iex(3)> GenServer.call(pid, {:set_alarm, self(), "alarm1", fn(_, data) -> data > 10 end})
    "Alarm added"

    / Enregistrement d'une alarme : le nom est déjà utilisé et cela provoque un erreur
    iex(4)> GenServer.call(pid, {:set_alarm, self(), "alarm1", fn x -> x end})
    {:error, "This name already exists"}

    // Ajout d'une donnée qui ne fera pas sonner l'alarme
    iex(5)> GenServer.cast(pid, {:data, 0, 7})
    :ok

    // Ajout d'une donnée qui fera sonner l'alarme
    iex(6)> GenServer.cast(pid, {:data, 0, 15})
    :ok

    // Vérification des alarmes levées
    iex(7)> receive do m -> m end
    {:alarm, "alarm1", 0, 15}

    // Rien à recevoir d'autre
    iex(8)> receive do m -> m end

    // Recherche des équipements qui feraient lever l'alarme "data < 100"
    // ==> À tester avec plusieurs équipements pour bien voir le fonctionnement
    iex(9)> GenServer.call(pid, {:search, fn(_, data) -> data < 100 end})
    [0]

  """
  def init(initCount) do
    {:ok, {initCount, Map.new}}
  end

  def handle_call({:register, infos}, _from, {count, dict}) do
    dict = Map.put(dict, count, %{:infos => infos, :data => []})
    {:reply, {"Equipment registered", count}, {count + 1, dict}}
  end

  def handle_call({:set_alarm, pid, name, filter_fun}, _from, {count, dict}) do
    if Map.has_key?(dict, name) do
      {:reply, {:error, "This name already exists"}, {count, dict}}
    else
      dict = Map.put(dict, name, %{:pid => pid, :filter_fun => filter_fun})
      {:reply, "Alarm added", {count, dict}}
    end
  end

  def handle_call({:search, filter_fun}, _from, {count, dict}) do
    {:reply, create_alarm_list(Map.keys(dict), filter_fun, dict), {count, dict}}
  end

  def handle_cast({:data, id, data}, {count, dict}) do
    if Map.has_key?(dict, id) do
      dict = Map.put(dict, id, Map.put(dict[id], :data, [data|dict[id][:data]]))
      Enum.each dict, fn {key, value} ->
        if is_binary key do
          if value[:filter_fun].(id, data) do
            send(value[:pid], {:alarm, key, id, data})
          end
        end
      end
      {:noreply, {count, dict}}
    else
      {:noreply, {count, dict}}
    end
  end

  def create_alarm_list [], _, _ do [] end
  def create_alarm_list [key|queue], function, dict do
    if is_integer key do
      if is_alarm_on function, key, dict[key][:data] do
        [key|create_alarm_list(queue, function, dict)]
      else
        create_alarm_list(queue, function, dict)
      end
    else
      create_alarm_list(queue, function, dict)
    end
  end

  def is_alarm_on _, _, [] do false end
  def is_alarm_on function, id, [data|queue] do function.(id, data) or is_alarm_on(function, id, queue) end
end

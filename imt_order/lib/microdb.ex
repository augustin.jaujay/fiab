defmodule MicroDb.HashTable do
  def put(db, key, val) do
    nbNodes = Application.fetch_env!(:imt_order, :nodes) |> Enum.count
    GenServer.call({__MODULE__, :"imt_order_#{nbNodes - 2}@127.0.0.1"},{:put, db, key, val})
  end

  def get(db, key) do
    nbNodes = Application.fetch_env!(:imt_order, :nodes) |> Enum.count
    GenServer.call({__MODULE__, :"imt_order_#{nbNodes - 2}@127.0.0.1"},{:get, db, key})
  end

  def hash(key) do
    key = :crypto.hash(:sha,:erlang.term_to_binary(key)) |> Base.encode16(case: :lower)
    <<hashstart::binary-size(2),hashrest::binary>> = key
    {hashstart,hashrest}
  end

  use GenServer

  def start_link(arg) do
    GenServer.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(_) do
    {:ok, nil}
  end

  def handle_call({:put, db, key, val}, _from, state) do
    {hashstart,hashrest} = hash(key)
    case File.write("data/#{db}/#{hashstart}/#{hashrest}",:erlang.term_to_binary(val)) do
      {:error, :enoent}->
        File.mkdir_p!("data/#{db}/#{hashstart}")
        File.write!("data/#{db}/#{hashstart}/#{hashrest}", :erlang.term_to_binary(val))
        {:reply, :ok, state}
      :ok-> {:reply, :ok, state}
    end
  end

  def handle_call({:get, db, key}, _from, state) do
    {hashstart,hashrest} = hash(key)
    case File.read("data/#{db}/#{hashstart}/#{hashrest}") do
      {:error,_}-> {:reply, nil, state}
      {:ok,bin}-> {:reply, :erlang.binary_to_term(bin), state}
    end
  end
end

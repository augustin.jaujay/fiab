defmodule ImtOrder.OrderTransactor do
  def start_link(order_id) do
    GenServer.start(__MODULE__, order_id, name: {:via, Registry, {OrderRegistry, "#{order_id}_transactor"}})
  end

  def send_request(url, order, counter) do
    # IO.inspect("#{url} : retrying #{counter} for order #{order["id"]}")
    if counter >= 3 do
      :ko
    else
      case :httpc.request(:post,{url,[],'application/json',Poison.encode!(order)},[],[]) do
        {:ok,{{_,code,_},_,_}} when code < 400 -> :ok
        {:ok,{{_,code,_},_,_}} ->
          :timer.sleep(2_000)
          send_request(url, order, counter + 1)
        {:error,reason} ->
          :timer.sleep(2_000)
          send_request(url, order, counter + 1)
        end
      end
  end

  use GenServer
  def init(id) do
    {:ok, id}
  end

  def handle_call({:order, order}, _from, state) do
    # IO.inspect {order["id"], "order"}
    selected_store = Enum.find(1..200,fn store_id->
      Enum.all?(order["products"],fn %{"id"=>prod_id,"quantity"=>q}->
        case MicroDb.HashTable.get("stocks",{store_id,prod_id}) do
          nil-> false
          store_q when store_q >= q-> true
          _-> false
        end
      end)
    end)
    order = Map.put(order,"store_id",selected_store)
    isOk = send_request('http://localhost:9091/order/new', order, 1)
    MicroDb.HashTable.put("orders",order["id"],order)
    {:reply, isOk, state}
  end

  def handle_call({:payment, transaction_id, orderid}, _from, state) do
    # IO.inspect {orderid, "payment"}
    case MicroDb.HashTable.get("orders",orderid) do
      nil-> {:reply, :ko, state}
      order->
        order = Map.put(order,"transaction_id",transaction_id)
        isOk = send_request('http://localhost:9091/order/process_delivery', order, 1)
        MicroDb.HashTable.put("orders",orderid,order)
        {:reply, isOk, state}
    end
  end
end

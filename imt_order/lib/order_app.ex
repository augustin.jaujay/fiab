defmodule ImtOrder.App do
  use Supervisor
  def start_link(_) do
    app_type = Application.fetch_env!(:imt_order, :app_type)
    {_host, httpPort} = Application.fetch_env!(:imt_order, :http)

    case app_type do
      "Stats" ->
        Supervisor.start_link([
          {MicroDb.HashTable, []},
          {ImtOrder.StatsToDb, []},
          ], strategy: :one_for_one)
      "LoadBalancer" ->
        Supervisor.start_link([
          {LoadBalancer, []}
        ], strategy: :one_for_one)
      _ ->
        Supervisor.start_link([
          {Plug.Cowboy, scheme: :http, plug: ImtOrder.API, options: [port: httpPort]},
          {ImtOrder.OrderSupervisor, []},
          {Registry, keys: :unique, name: OrderRegistry}
        ], strategy: :one_for_one)
      end
  end
end

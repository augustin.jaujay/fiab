[
  imt_order: [
    http: (case (Application.get_env(:distmix, :nodes) |> Enum.count) - DistMix.nodei() do
      1 -> {"127.0.0.1", 9091}
      3 -> {"127.0.0.1", 9090}
      _ -> {"127.0.0.1", 1 + DistMix.add_nodei(9090)}
    end),
    nodes: Application.get_env(:distmix, :nodes),
    app_type: (case (Application.get_env(:distmix, :nodes) |> Enum.count) - DistMix.nodei() do
      0 -> "Front"
      1 -> "Back"
      2 -> "Stats"
      3 -> "LoadBalancer"
      _ -> "APITransactor"
    end),
    node_index: DistMix.nodei()
  ]
]

defmodule ImtOrder.App do
  use Supervisor
  def start_link(_) do
    Supervisor.start_link([
        {Plug.Cowboy, scheme: :http, plug: ImtOrder.API, options: [port: 9090]},
        ImtOrder.StatsAggregate
      ], strategy: :one_for_one)
  end
end

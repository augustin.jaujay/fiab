defmodule ImtOrder.StatsAsDb do
  def find_bisec(file,id_bin) do
    {id, ""} = Integer.parse(id_bin)
    f = File.open!(file)
    {:ok,file_info} = :file.read_file_info(file)
    vals = bisec(f,id,0,elem(file_info,1))
    :ok = File.close(f)
    vals
  end

  def bisec(f,id,min,max) do
    split_pos = min+div(max-min,2)
    {:ok,_} = :file.position(f,{:bof,split_pos})
    data = IO.binread(f,30)
    [before,bin|_rest] = String.split(data,"\n", parts: 3)
    # edge case : bof
    bin = if min == 0 and max < 6 do before else bin end
    case bin do
      ""-> # edge case : eof
        bisec(f,id,min,split_pos)
      _->
        [line_id_bin|rest] = String.split(bin,",")
        {line_id,""} = Integer.parse(line_id_bin)
        case line_id do
          ^id-> rest
          x when x > id-> bisec(f,id,split_pos+byte_size(before)+byte_size(bin),max)
          _ -> bisec(f,id,min,split_pos)
        end
    end
  end

  def find_enum(file,id_bin) do
    File.stream!(file) |> Enum.find_value(fn line->
      case line |> String.trim_trailing("\n") |> String.split(",") do
        [^id_bin|rest]->rest
        _-> nil
      end
    end)
  end
end


defmodule ImtOrder.StatsAggregate do
  use GenServer

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    Process.send_after(self(), :aggregate, 2_000)
    state = Enum.reduce(1..10_000, %{}, fn product_nb, acc ->
      acc = Map.put(acc, product_nb, [])
    end)
    {:ok, state}
  end

  def handle_cast(:stop, state) do
    {:stop, :normal, state}
  end

  def handle_call({:get, prod_id}, _from, state) do
    {:reply, state[String.to_integer(prod_id)], state}
  end

  def handle_info(:aggregate, state) do
    res = Path.wildcard("data/stat_*")
    |> Enum.reduce(state, fn(file_name, acc1) ->
      f = File.open!(file_name)
      data = IO.binread(f, :all)
      new_acc1 = Regex.split(~r{\n}, data)
      |> Enum.reduce(acc1, fn(line, acc2) ->
        line_prod = Regex.split(~r{,}, line, parts: 3)
        id = String.to_integer(Enum.at(line_prod, 0), 10)
        quantity = String.to_integer(Enum.at(line_prod, 1), 10)
        price = String.to_integer(Enum.at(line_prod, 2), 10)
        MicroDb.HashTable.put("stats", id, {quantity, price})
        Map.update!(acc2, id, fn c -> [{quantity, price}|c] end)
      end)
      File.close(f)
      File.rm(f)

      # IO.inspect(new_acc1)
      new_acc1
    end)

    Process.send_after(self(), :aggregate, 10_000)
    {:noreply, res}
  end
end

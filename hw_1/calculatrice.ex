:illegal_char
:plus
:minus
:times
:divided
:equal
:diff
:greater
:lower
:geq
:leq

:int
:var

defmodule Calculator do
    def next_token [] do [] end
    def next_token ["!", "="|q] do {:diff, q} end
    def next_token ["<", "="|q] do {:leq, q} end
    def next_token [">", "="|q] do {:geq, q} end
    def next_token ["+"|q] do {:plus, q} end
    def next_token ["-"|q] do {:minus, q} end
    def next_token ["*"|q] do {:times, q} end
    def next_token ["/"|q] do {:divided, q} end
    def next_token ["="|q] do {:equal, q} end
    def next_token ["<"|q] do {:lower, q} end
    def next_token [">"|q] do {:greater, q} end
    def next_token [a|q] do
        if Integer.parse(a) != :error do {:int, elem(Integer.parse(read_int([a|q])), 0)}
        else {:var, read_var([a|q])}
        end
    end
    def next_token [a|q] do {:illegal_char, a} end

    def read_int [] do "" end
    def read_int [a|q] do a <> read_int(q) end

    def read_var [] do "" end
    def read_var ["!", "="|_] do "" end
    def read_var ["<", "="|_] do "" end
    def read_var [">", "="|_] do "" end
    def read_var ["+"|_] do "" end
    def read_var ["-"|_] do "" end
    def read_var ["*"|_] do "" end
    def read_var ["/"|_] do "" end
    def read_var ["="|_] do "" end
    def read_var ["<"|_] do "" end
    def read_var [">"|_] do "" end
    def read_var [a|q] do if Integer.parse(a) === :error do a <> read_var(q) else "" end
    end
end

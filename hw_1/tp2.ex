defmodule Tokenize do
    def tokenize "" do [] end
    def tokenize a do next_token(String.codepoints(a)) end

    def next_token [] do [] end
    def next_token [" "|q] do next_token(q) end
    def next_token ["("|q] do [:open|next_token(q)] end
    def next_token [")"|q] do [:close|next_token(q)] end
    def next_token ["!", "="|q] do [:diff|next_token(q)] end
    def next_token ["<", "="|q] do [:leq|next_token(q)] end
    def next_token [">", "="|q] do [:geq|next_token(q)] end
    def next_token ["+"|q] do [:plus|next_token(q)] end
    def next_token ["-"|q] do [:minus|next_token(q)] end
    def next_token ["*"|q] do [:times|next_token(q)] end
    def next_token ["/"|q] do [:divided|next_token(q)] end
    def next_token ["="|q] do [:equal|next_token(q)] end
    def next_token ["<"|q] do [:lower|next_token(q)] end
    def next_token [">"|q] do [:greater|next_token(q)] end
    def next_token [a|q] do
        if Integer.parse(a) != :error do
        	[elem(Integer.parse(read_int([a|q])), 0)|next_token(get_int_queue([a|q]))]
        else [read_var([a|q])|next_token(get_var_queue([a|q]))]
        end
    end

    def get_int_queue [] do [] end
    def get_int_queue [a|q] do
    	if Integer.parse(a) != :error do get_int_queue(q)
    	else [a|q]
    	end
    end

    def get_var_queue [] do [] end
    def get_var_queue [a|q] do
    	if Integer.parse(a) == :error do get_var_queue(q)
    	else [a|q]
    	end
    end

    def read_int [] do "" end
    def read_int [a|q] do a <> read_int(q) end

    def read_var [] do "" end
    def read_var [" "|_] do "" end
    def read_var ["("|_] do "" end
    def read_var [")"|_] do "" end
    def read_var ["!", "="|_] do "" end
    def read_var ["<", "="|_] do "" end
    def read_var [">", "="|_] do "" end
    def read_var ["+"|_] do "" end
    def read_var ["-"|_] do "" end
    def read_var ["*"|_] do "" end
    def read_var ["/"|_] do "" end
    def read_var ["="|_] do "" end
    def read_var ["<"|_] do "" end
    def read_var [">"|_] do "" end
    def read_var [a|q] do if Integer.parse(a) === :error do a <> read_var(q) else "" end
    end
end

defmodule Parse do
    def parse_expr [] do :empty end
    def parse_expr([a]) when is_integer(a) do {:empty, a, :empty} end
    def parse_expr [:open|q] do parse_expr(parse_val(0, [:open|q])) end
    def parse_expr l do
        case remaining_val(0, l) do
            [] -> {:empty, parse_val(0, l), :empty}
            [:times|q] -> {parse_expr(parse_val(0, l)), :times, parse_expr(parse_val(0, q))}
            [:plus|q] -> {parse_expr(parse_val(0, l)), :plus, parse_expr(parse_val(0, q))}
            [:minus|q] -> {parse_expr(parse_val(0, l)), :minus, parse_expr(parse_val(0, q))}
            [:divided|q] -> {parse_expr(parse_val(0, l)), :divided, parse_expr(parse_val(0, q))}
            [:leq|q] -> {parse_expr(parse_val(0, l)), :leq, parse_expr(parse_val(0, q))}
            [:geq|q] -> {parse_expr(parse_val(0, l)), :geq, parse_expr(parse_val(0, q))}
            [:lower|q] -> {parse_expr(parse_val(0, l)), :lower, parse_expr(parse_val(0, q))}
            [:greater|q] -> {parse_expr(parse_val(0, l)), :greater, parse_expr(parse_val(0, q))}
            [:equal|q] -> {parse_expr(parse_val(0, l)), :equal, parse_expr(parse_val(0, q))}
            [:diff|q] -> {parse_expr(parse_val(0, l)), :diff, parse_expr(parse_val(0, q))}
        end
    end

    def remaining_val _, [] do [] end
    def remaining_val 1, [:close|q] do q end
    def remaining_val(count, [:close|q]) when count > 0 do remaining_val(count - 1, q) end
    def remaining_val count, [:open|q] do remaining_val(count + 1, q) end
    def remaining_val(count, [_|q]) when count > 0 do remaining_val(count, q) end
    def remaining_val(_, [a|q]) when is_integer(a) do q end
    def remaining_val count, [_|q] do remaining_val(count, q) end

    def parse_val _, [] do [] end
    def parse_val 1, [:close|_] do [] end
    def parse_val(count, [:close|q]) when count > 0 do [:close|parse_val(count - 1, q)] end
    def parse_val(count, [:open|q]) when count > 0 do [:open|parse_val(count + 1, q)] end
    def parse_val count, [:open|q] do parse_val(count + 1, q) end
    def parse_val(count, [a|q]) when count > 0 do [a|parse_val(count, q)] end
    def parse_val(_,  [a|_]) when is_integer(a) do [a] end
    def parse_val _,  [a|_] do [a] end
end

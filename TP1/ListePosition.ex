:empty

defmodule M do
    def create [] do {[], nil, []} end
    def create [a|q] do {[], a, q} end

    def get_current {_, v, _} do v end

    def move_left {[], v, d} do {[], v, d} end
    def move_left {[g|q], v, d} do {q, g, [v|d]} end

    def move_right {g, v, []} do {g, v, []} end
    def move_right {g, v, [d|q]} do {[v|g], d, q} end

    def add_left x, :empty do {[], x, []} end
    def add_left x, {g, v, d} do {[x|g], v, d} end

    def add_right x, :empty do {[], x, []} end
    def add_right x, {g, v, d} do {g, v, [x|d]} end
end

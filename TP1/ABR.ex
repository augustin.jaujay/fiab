:empty

defmodule M do
    def add(val, :empty) do {:empty, val, :empty} end
    def add(new_val, {arbre_g, val, arbre_d}) when new_val < val do {add(new_val, arbre_g), val, arbre_d} end
    def add(new_val, {arbre_g, val, arbre_d}) do {arbre_g, val, add(new_val, arbre_d)} end

    def list_to_tree([]) do :empty end
    def list_to_tree([a|q]) do add(a, list_to_tree(q)) end

    def tree_to_list(:empty) do [] end
    def tree_to_list({arbre_g, val, arbre_d}) do tree_to_list(arbre_g) ++ [val | tree_to_list(arbre_d)] end

    def sort(l) do tree_to_list(list_to_tree(l))end
end

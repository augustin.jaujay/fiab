defmodule ImtOrder.App do
  use Supervisor
  def start_link(_) do
    [name, host] = String.split(Atom.to_string(Node.self), "@")

    if name == "sim" do
      Supervisor.start_link([
        {Plug.Cowboy, scheme: :http, plug: ImtOrder.API, options: [port: 9090]},
        {ImtOrder.StatsToDb, []},
        {ImtOrder.OrderSupervisor, []},
        {Registry, keys: :unique, name: OrderRegistry}
        ], strategy: :one_for_one)
      else
        Node.connect(:"sim@#{host}")
        Supervisor.start_link([
          {ImtOrder.OrderSupervisor, []},
          {Registry, keys: :unique, name: OrderRegistry}
        ], strategy: :one_for_one)
      end
  end
end

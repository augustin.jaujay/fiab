defmodule ImtOrder.OrderSupervisor do
  def start_server(order_id, node) do
    case DynamicSupervisor.start_child({__MODULE__, node}, {ImtOrder.OrderTransactor, order_id}) do
      {:error, {:already_started, pid}} -> {:existing, pid}
      {:ok, pid} -> {:new, pid}
      error -> :error
    end
  end

  def stop_server(pid) do
    DynamicSupervisor.terminate_child(__MODULE__, pid)
  end

  use DynamicSupervisor

  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end
